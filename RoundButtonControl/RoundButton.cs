﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace RoundButtonControl
{
    [TemplatePart(Name = "PART_grid", Type = typeof(Grid))]
    public class RoundButton : ContentControl, ICommandSource
    {
        Grid grid;
        static readonly DependencyPropertyKey isPressedPropertyKey;

        public static readonly DependencyProperty IsPressedProperty;
        public static readonly DependencyProperty CommandProperty;
        public static readonly DependencyProperty CommandParameterProperty;
        public static readonly DependencyProperty CommandTargetProperty;

        static RoundButton()
        {
            Type thisType = typeof(RoundButton);
            
            DefaultStyleKeyProperty.OverrideMetadata(thisType, new FrameworkPropertyMetadata(thisType));

            isPressedPropertyKey = DependencyProperty.RegisterReadOnly("IsPressed", typeof(bool), thisType, new PropertyMetadata(false));
            IsPressedProperty = isPressedPropertyKey.DependencyProperty;
            CommandProperty = DependencyProperty.Register("Command", typeof(ICommand), thisType);
            CommandParameterProperty = DependencyProperty.Register("CommandParameter", typeof(object), thisType);
            CommandTargetProperty = DependencyProperty.Register("CommandTarget", typeof(IInputElement), thisType,
                new FrameworkPropertyMetadata(null));
        }

        [ReadOnly(true)]
        public bool IsPressed
        {
            get => (bool)GetValue(IsPressedProperty);
            private set => SetValue(isPressedPropertyKey, value);
        }

        public ICommand Command
        {
            get => (ICommand)GetValue(CommandProperty);
            set => SetValue(CommandProperty, value);
        }

        public object CommandParameter
        {
            get => GetValue(CommandParameterProperty);
            set => SetValue(CommandParameterProperty, value);
        }

        public IInputElement CommandTarget
        {
            get => (IInputElement)GetValue(CommandTargetProperty);
            set => SetValue(CommandTargetProperty, value);
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            IsPressed = true;

            if (Command != null)
            {
                if (Command is RoutedCommand routedCommand)
                    routedCommand.Execute(CommandParameter, CommandTarget);
                else
                    Command.Execute(CommandParameter);
            }
        }

        private void Grid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            IsPressed = false;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            grid = (Grid)GetTemplateChild("PART_grid");
            grid.MouseLeftButtonDown += Grid_MouseLeftButtonDown;
            grid.MouseLeftButtonUp += Grid_MouseLeftButtonUp;
        }
    }
}