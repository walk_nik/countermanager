﻿namespace CounterManager.Common
{
    static class RegionNames
    {
        public static readonly string ButtonRegion = nameof(ButtonRegion);
        public static readonly string CountersFirstRegion = nameof(CountersFirstRegion);
        public static readonly string CountersSecondRegion = nameof(CountersSecondRegion);
    }
}