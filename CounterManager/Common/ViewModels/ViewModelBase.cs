﻿using Prism.Events;
using Prism.Mvvm;

namespace CounterManager.Common.ViewModels
{
    class ViewModelBase : BindableBase
    {
        protected readonly IEventAggregator eventAgregator;

        protected ViewModelBase(IEventAggregator eventAgregator)
        {
            this.eventAgregator = eventAgregator;
        }
    }
}