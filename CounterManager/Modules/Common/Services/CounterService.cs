﻿namespace CounterManager.Modules.Common.Services
{
    interface ICounterService
    {
        uint Value { get; }
    }

    interface ICounterIncreaseService : ICounterService
    {
        uint IncreaseByOne();
    }

    interface ICounterDecreaseService : ICounterService
    {
        uint DecreaseByOne();
    }

    class CounterService : ICounterIncreaseService, ICounterDecreaseService
    {
        public uint Value { get; private set; }

        public CounterService(uint initialValue = 0) => Value = initialValue;

        public uint IncreaseByOne() => ++Value;

        public uint DecreaseByOne() => --Value;
    }
}