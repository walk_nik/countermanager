﻿using Prism.Events;

namespace CounterManager.Modules.Common.Events
{
    class CounterIncreaseEvent : PubSubEvent { }
}