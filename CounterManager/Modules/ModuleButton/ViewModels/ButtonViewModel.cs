﻿using CounterManager.Common.ViewModels;
using CounterManager.Modules.Common.Events;
using CounterManager.Modules.Common.Services;
using Prism.Commands;
using Prism.Events;

namespace CounterManager.Modules.ModuleButton.ViewModels
{
    class ButtonViewModel : ViewModelBase
    {
        readonly CounterIncreaseEvent counterIncreaseEvent;
        readonly ICounterIncreaseService counterService;

        public DelegateCommand ButtonClickCommand { get; }

        public ButtonViewModel(IEventAggregator eventAgregator, ICounterIncreaseService counterService) : 
            base(eventAgregator)
        {
            ButtonClickCommand = new DelegateCommand(ButtonClickCommandHandler);
            counterIncreaseEvent = eventAgregator.GetEvent<CounterIncreaseEvent>();

            this.counterService = counterService;
        }

        void ButtonClickCommandHandler()
        {
            counterService.IncreaseByOne();
            counterIncreaseEvent.Publish();
        }
    }
}