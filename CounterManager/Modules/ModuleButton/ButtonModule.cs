﻿using CounterManager.Common;
using CounterManager.Modules.ModuleButton.ViewModels;
using CounterManager.Modules.ModuleButton.Views;
using Ninject;
using Prism.Regions;

namespace CounterManager.Modules.ModuleButton
{
    class ButtonModule : ModuleBase
    {
        public ButtonModule(IKernel kernel, IRegionManager regionManager) : 
            base(kernel, regionManager) { }

        public override void Initialize()
        {
            ButtonViewModel buttonViewModel = kernel.Get<ButtonViewModel>();
            ButtonView buttonView = kernel.Get<ButtonView>();

            buttonView.DataContext = buttonViewModel;

            regionManager.Regions[RegionNames.ButtonRegion].Add(buttonView);
        }
    }
}