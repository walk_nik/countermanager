﻿using CounterManager.Common.ViewModels;
using CounterManager.Modules.Common.Events;
using CounterManager.Modules.Common.Services;
using Prism.Commands;
using Prism.Events;

namespace CounterManager.ViewModels
{
    class CountersViewModel : ViewModelBase
    {
        uint counter;
        readonly ICounterDecreaseService counterService;
        public DelegateCommand DecreaseValueCommand { get; }

        public CountersViewModel(IEventAggregator eventAgregator, ICounterDecreaseService counterService) : 
            base(eventAgregator)
        {
            DecreaseValueCommand = new DelegateCommand(DecreaseValueCommandHandler, () => Counter != 0);
            eventAgregator.GetEvent<CounterIncreaseEvent>().Subscribe(CounterIncreaseEventHandler);

            this.counterService = counterService;
        }

        public uint Counter
        {
            get => counter;
            set
            {
                counter = value;
                RaisePropertyChanged();
            }
        }

        void CounterIncreaseEventHandler()
        {
            Counter = counterService.Value;

            if (Counter == 1)
                DecreaseValueCommand.RaiseCanExecuteChanged();
        }

        void DecreaseValueCommandHandler()
        {
            Counter = counterService.DecreaseByOne();

            if (Counter == 0)
                DecreaseValueCommand.RaiseCanExecuteChanged();
        }
    }
}