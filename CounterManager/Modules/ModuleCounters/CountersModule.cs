﻿using CounterManager.Common;
using CounterManager.Modules.ModuleCounters.Views;
using CounterManager.ViewModels;
using Ninject;
using Prism.Regions;

namespace CounterManager.Modules.ModuleCounters
{
    class CountersModule : ModuleBase
    {
        public CountersModule(IKernel kernel, IRegionManager regionManager) : 
            base(kernel, regionManager) { }

        public override void Initialize()
        {
            CountersViewModel countersViewModel = kernel.Get<CountersViewModel>();
            CountersView countersFirstView = kernel.Get<CountersView>();
            CountersView countersSecondView = kernel.Get<CountersView>();

            countersFirstView.DataContext = countersViewModel;
            countersSecondView.DataContext = countersViewModel;

            regionManager.Regions[RegionNames.CountersFirstRegion].Add(countersFirstView);
            regionManager.Regions[RegionNames.CountersSecondRegion].Add(countersSecondView);
        }
    }
}