﻿using Ninject;
using Prism.Modularity;
using Prism.Regions;

namespace CounterManager.Modules
{
    abstract class ModuleBase : IModule
    {
        protected readonly IKernel kernel;
        protected readonly IRegionManager regionManager;

        protected ModuleBase(IKernel kernel, IRegionManager regionManager)
        {
            this.kernel = kernel;
            this.regionManager = regionManager;
        }

        public abstract void Initialize();
    }
}