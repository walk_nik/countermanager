﻿using System.Windows;
using CounterManager.Common.Views;
using CounterManager.Modules.Common.Services;
using CounterManager.Modules.ModuleButton;
using CounterManager.Modules.ModuleCounters;
using Ninject;
using Prism.Modularity;
using Prism.Ninject;

namespace CounterManager
{
    class Bootstrapper : NinjectBootstrapper
    {
        protected override DependencyObject CreateShell()
        {
            return Kernel.Get<Shell>();
        }

        protected override void InitializeShell()
        {
            Application.Current.MainWindow = (Window)Shell;
            Application.Current.MainWindow.Show();

            base.InitializeShell();
        }

        protected override void ConfigureKernel()
        {
            Kernel.Bind<ICounterIncreaseService, ICounterDecreaseService>().To<CounterService>().InSingletonScope();

            base.ConfigureKernel();
        }

        protected override void InitializeModules()
        {
            IModule buttonModule = Kernel.Get<ButtonModule>();
            IModule countersModule = Kernel.Get<CountersModule>();

            buttonModule.Initialize();
            countersModule.Initialize();

            base.InitializeModules();
        }
    }
}